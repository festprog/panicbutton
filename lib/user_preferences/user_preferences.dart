import 'package:shared_preferences/shared_preferences.dart';

class PreferencesUser {
  static final PreferencesUser _instancia = PreferencesUser._internal();

  factory PreferencesUser() {
    return _instancia;
  }

  PreferencesUser._internal();

  late SharedPreferences _prefs;

  initPrefs() async {
    _prefs = await SharedPreferences.getInstance();
  }

  set token(String value) {
    _prefs.setString('token', value);
  }

  String get token {
    return _prefs.getString('token') ?? "";
  }

  set user(String value) {
    _prefs.setString(('user'), value);
  }

  String get user {
    return _prefs.getString('user') ?? "";
  }

  // ESCRIBIR SOBRE UNA VARIABLE
  set email(String value) {
    _prefs.setString('email', value);
  }

  String get email {
    return _prefs.getString('email') ?? "";
  }

  set name(String value) {
    _prefs.setString('name', value);
  }

  String get name {
    return _prefs.getString('name') ?? "";
  }

  set lastname(String value) {
    _prefs.setString('lastname', value);
  }

  String get lastname {
    return _prefs.getString('lastname') ?? "";
  }

  set password(String value) {
    _prefs.setString('password', value);
  }

  String get password {
    return _prefs.getString('password') ?? "";
  }
}
