import 'package:flutter/material.dart';
import 'package:v1botonpanico/theme/theme.dart';
import 'package:v1botonpanico/widgets/widget1_login.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({super.key});

  // VARIABLES - INSTANCIAS
  // ---

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // PAGINA PRINCIPAL
      backgroundColor: const Color(0xFFFDFDFD),
      appBar: AppBar(
        // TITULO BARRA SUPERIOR
        title: const Text('Iniciar Sesión'),
        centerTitle: true,
        backgroundColor: const Color(0x0F4C5C | mask),
        elevation: 0,
      ),
      body: LoginWidget(),
    );
  }
}
