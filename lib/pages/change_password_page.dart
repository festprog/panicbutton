import 'package:v1botonpanico/theme/theme.dart';
import 'package:v1botonpanico/widgets/change_password_widget.dart';
import 'package:flutter/material.dart';

class ChangePasswordPage extends StatelessWidget {
  const ChangePasswordPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFFDFDFD),
      appBar: AppBar(
        title: const Text('Cambiar contraseña'),
        centerTitle: true,
        backgroundColor: const Color(0x0F4C5C | mask),
        elevation: 0,
      ),
      body: const Padding(
        padding: EdgeInsets.all(0),
        child: Center(child: ChangePasswordWidget()),
      ),
    );
  }
}
