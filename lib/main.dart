import 'package:flutter/material.dart';
import 'package:v1botonpanico/pages/change_password_page.dart';
import 'package:v1botonpanico/pages/edit_profile_page.dart';
import 'package:v1botonpanico/pages/page1_login.dart';
import 'package:v1botonpanico/pages/page2_sigin.dart';
import 'package:v1botonpanico/pages/page3_check.dart';
import 'package:v1botonpanico/pages/page4_eventreport.dart';
import 'package:v1botonpanico/user_preferences/user_preferences.dart';
import 'package:v1botonpanico/widgets/fake_home_page.dart';
import 'package:v1botonpanico/widgets/find_location.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final prefs = PreferencesUser();
  await prefs.initPrefs();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // VARIABLES
  // --

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Pagina principal',
      initialRoute: "login",
      routes: {
        "login": (context) => const LoginPage(),
        "sigin": (context) => const SiginPage(),
        "codigo": (context) => const checkPage(),
        "eventReport": (context) => const eventreportPage(),
        "editprofile": (context) => const EditProfilePage(),
        "changepass": (context) => const ChangePasswordPage(),
        "location": (context) => const FindLocation(),
        "fakehome": (context) => const FakeHomePage(),
      },
    );
  }
}
