import 'dart:convert';
import 'package:flutter/material.dart';
import "package:http/http.dart" as http;
import 'package:v1botonpanico/user_preferences/user_preferences.dart';

class ReportServices {
  // VARIABLES - INSTANCIAS
  final prefs = PreferencesUser();
  final ip = "http://sistemic.udea.edu.co:4000/reto";

  // FUNCIÓN PARA AGREGAR UN REPORTE
  Future report(List<double> location, String eventDescription, String comment,
      String username, BuildContext context) async {
    var headers = {
      'Authorization': 'Bearer ${prefs.token}',
      // aqui esta el error , no estas pasando el token
      'Content-Type': 'application/json'
    };
    var request = http.Request(
        'POST', Uri.parse('$ip/events/eventos/crear/usuario/$username'));
    request.body = json.encode({
      "location": location,
      "eventDescription": eventDescription,
      "comment": comment
    });
    request.headers.addAll(headers);

    try {
      http.StreamedResponse response =
          await request.send().timeout(const Duration(seconds: 10));

      if (response.statusCode == 201) {
        // Se usa el 201, es un código especifico para indicar que se creo exitosamente el POST
        // Verificado con Postman
        Navigator.pushNamed(context, "eventReport");
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return const AlertDialog(
                title: Text('Evento registrado'),
                content: Text('Gracias por tu colaboración'),
                contentPadding: EdgeInsets.all(20),
              );
            });
        debugPrint(await response.stream.bytesToString());
      } else {
        debugPrint(response.reasonPhrase);
      }
    } catch (e) {
      debugPrint("$e");
    }
  }
}
